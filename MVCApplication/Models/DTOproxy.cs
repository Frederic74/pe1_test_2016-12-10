﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCApplication.Models
{

    public class CategorieDTOproxy
    {
        public int Id { get; set; }
        public string Categorie { get; set; }
        //Categorie1 te gebruiken ?
    }

    public class ArtikelDTOproxy
    {
        public short Artikel_id { get; set; }
        public short Cat_id { get; set; }
        public string Artikel1 { get; set; }
        public string Omschrijving { get; set; }
        public decimal Verkoopprijs { get; set; }
        public short Instock { get; set; }

        public string Categorie { get; set; }
    }

}



