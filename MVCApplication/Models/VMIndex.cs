﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCApplication.Models;    //Toegevoegd

namespace MVCApplication.Models
{
    public class VMIndex
    {

        //Deze properties worden gevuld in de controller (HomeController)
        public IEnumerable<CategorieDTOproxy> CategoriesVM { get; set; }
        public IEnumerable<ArtikelDTOproxy> ArtikelsVM { get; set; }
        public CategorieDTOproxy HuidigeCategorieVM { get; set; }

    }
}