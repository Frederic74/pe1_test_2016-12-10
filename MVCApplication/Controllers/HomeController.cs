﻿


//HomeController van MVCApplication



using Newtonsoft.Json;          //Toegevoegd om JsonConvert te kunnen gebruiken
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;          //Toegevoegd om HttpClient te kunnen gebruiken
using System.Threading.Tasks;   //Toegevoegd om Task te kunnen gebruiken
using System.Web;
using System.Web.Mvc;
using MVCApplication.Models;    //Toegevoegd



namespace MVCApplication.Controllers
{
    public class HomeController : Controller
    {

        //public ActionResult Index()
        //{
        //    return View();
        //}

        // GET: Home
        //      De method Index behandelt de standaard URL voor de HomeController klasse “/”
        //          get .../
        //          get .../Home
        //public ActionResult Index()
        //{
        //    string uri = "http://" + Request.Url.Host + ':' + "62989" + "/api/categories";
        //    //poortnummer van WebApi Request.Url.Port toevoegen in URL
        //    using (HttpClient httpClient = new HttpClient())
        //    {
        //        Task<String> response = httpClient.GetStringAsync(uri);
        //        return View(Task.Factory.StartNew(() => JsonConvert.DeserializeObject<List<CategorieDTOproxy>>(response.Result)).Result);
        //    }
        //}

        // GET: Home
        //      De method Index behandelt de standaard URL voor de HomeController klasse “/”
        //          get .../
        //          get .../Home
        public ActionResult Index()
        {
            string uri = "http://" + Request.Url.Host + ':' + "62989" + "/api/categories";
            //poortnummer van WebApi Request.Url.Port toevoegen in URL
            
            //ViewModel aanmaken:
            VMIndex vm = new VMIndex();
                //public IEnumerable<CategorieDTOproxy> CategoriesVM { get; set; }
                //public IEnumerable<ArtikelDTOproxy> ArtikelsVM { get; set; }
                //public CategorieDTOproxy HuidigeCategorieVM { get; set; }

            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);
                vm.CategoriesVM = Task.Factory.StartNew(() => JsonConvert.DeserializeObject<List<CategorieDTOproxy>>(response.Result)).Result;
                return View(vm);
            }
        }




        //public PartialViewResult Artikelen()
        //{
        //    string uri = "http://" + Request.Url.Host + ':' + "62989" + "/api/categories/3";
        //    //poortnummer van WebApi Request.Url.Port toevoegen in URL
        //    using (HttpClient httpClient = new HttpClient())
        //    {
        //        Task<String> response = httpClient.GetStringAsync(uri);
        //        return PartialView(Task.Factory.StartNew(() => JsonConvert.DeserializeObject<List<ArtikelDTOproxy>>(response.Result)).Result);
        //    }
        //}

        //http://localhost:59432/Home/Artikelen?pId=2
        //Deze methode ontvangt een categorienummer en retourneert een PartialView.
        //[HttpPost]
        public PartialViewResult Artikelen(int pId)
        {
            if (pId > 0)
            {
                //System.Threading.Thread.Sleep(1000); //Simuleren wachttijd
                string uri = "http://" + Request.Url.Host + ':' + "62989" + "/api/categories/" + pId;
                //poortnummer van WebApi Request.Url.Port toevoegen in URL
                using (HttpClient httpClient = new HttpClient())
                {
                    Task<String> response = httpClient.GetStringAsync(uri);
                    return PartialView(Task.Factory.StartNew(() => JsonConvert.DeserializeObject<List<ArtikelDTOproxy>>(response.Result)).Result);
                }
            }
            else
            {
                return null;
            }
        }

        ////http://localhost:59432/Home/Artikelen?pId=2
        ////Deze methode ontvangt een categorienummer en retourneert een JsonResult.
        //[HttpPost]
        //public JsonResult Artikelen(int pId)
        //{
        //    System.Threading.Thread.Sleep(1000); //Simuleren wachttijd
        //    if (pId > 0)
        //    {
        //        string uri = "http://" + Request.Url.Host + ':' + "62989" + "/api/categories/" + pId;
        //        //poortnummer van WebApi toevoegen in URL
        //        using (HttpClient httpClient = new HttpClient())
        //        {
        //            Task<String> response = httpClient.GetStringAsync(uri);
        //            return Json(Task.Factory.StartNew(() => JsonConvert.DeserializeObject<List<ArtikelDTOproxy>>(response.Result)).Result, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    else
        //    {
        //        return Json(new { /* Empty object */ });
        //    }
        //}

            

        //http://localhost:59432/Home/Artikeldetails?pId=2
        //Deze methode ontvangt een categorienummer en retourneert een PartialView.
        //[HttpPost]
        public PartialViewResult Artikeldetails(int pId)
        {
            if (pId > 0)
            {
                //System.Threading.Thread.Sleep(1000); //Simuleren wachttijd
                string uri = "http://" + Request.Url.Host + ':' + "62989" + "/api/artikels/" + pId;
                //poortnummer van WebApi Request.Url.Port toevoegen in URL
                using (HttpClient httpClient = new HttpClient())
                {
                    Task<String> response = httpClient.GetStringAsync(uri);
                    return PartialView(Task.Factory.StartNew(() => JsonConvert.DeserializeObject<ArtikelDTOproxy>(response.Result)).Result);
                }
            }
            else
            {
                return null;
            }
        }



        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}