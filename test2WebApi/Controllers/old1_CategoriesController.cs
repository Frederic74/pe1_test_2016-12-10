﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Data.Entity.Infrastructure;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Threading.Tasks;
//using System.Web.Http;
//using System.Web.Http.Description;
//using test2WebApi.Models;
//using WebApiLib;

//namespace test2WebApi.Controllers
//{
//    public class CategoriesController : ApiController
//    {
//        private SpionshopEntities db = new SpionshopEntities();

//        // GET: api/Categories
//        public IQueryable<CategorieDTO> GetCategories()
//        {
//            var categories = from b in db.Categories
//                             select new CategorieDTO()
//                             {
//                                 Id = b.Cat_id,
//                                 Categorie = b.Categorie1
//                             };

//            return categories;
//        }



//        //// GET: api/Categories/5
//        //[ResponseType(typeof(Categorie))]
//        //public IHttpActionResult GetCategorie(short id)
//        //{
//        //    Categorie categorie = db.Categories.Find(id);
//        //    if (categorie == null)
//        //    {
//        //        return NotFound();
//        //    }

//        //    return Ok(categorie);
//        //}

//        // GET: api/Categories/5
//        [ResponseType(typeof(CategorieDTO))]
//        public async Task<IHttpActionResult> GetCategorie(int id)
//        {
//            var categorie = await (from b in db.Categories
//                                   select new CategorieDTO()
//                                   {
//                                       Id = b.Cat_id,
//                                       Categorie = b.Categorie1
//                                   }).SingleOrDefaultAsync(b => b.Id == id);

//            if (categorie == null)
//            {
//                return NotFound();
//            }

//            return Ok(categorie);
//        }



//        // PUT: api/Categories/5
//        [ResponseType(typeof(void))]
//        public IHttpActionResult PutCategorie(short id, Categorie categorie)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }

//            if (id != categorie.Cat_id)
//            {
//                return BadRequest();
//            }

//            db.Entry(categorie).State = EntityState.Modified;

//            try
//            {
//                db.SaveChanges();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!CategorieExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }

//            return StatusCode(HttpStatusCode.NoContent);
//        }

//        // POST: api/Categories
//        [ResponseType(typeof(Categorie))]
//        public IHttpActionResult PostCategorie(Categorie categorie)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }

//            db.Categories.Add(categorie);
//            db.SaveChanges();

//            return CreatedAtRoute("DefaultApi", new { id = categorie.Cat_id }, categorie);
//        }

//        // DELETE: api/Categories/5
//        [ResponseType(typeof(Categorie))]
//        public IHttpActionResult DeleteCategorie(short id)
//        {
//            Categorie categorie = db.Categories.Find(id);
//            if (categorie == null)
//            {
//                return NotFound();
//            }

//            db.Categories.Remove(categorie);
//            db.SaveChanges();

//            return Ok(categorie);
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                db.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        private bool CategorieExists(short id)
//        {
//            return db.Categories.Count(e => e.Cat_id == id) > 0;
//        }
//    }
//}