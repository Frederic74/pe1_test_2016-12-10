﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using test2WebApi.Models;   //Toegevoegd om DTO klassen te kunnen aanspreken
using WebApiLib;

namespace test2WebApi.Controllers
{
    public class CategoriesController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();
        


        //// GET: api/Categories
        //public IQueryable<Categorie> GetCategories()
        //{
        //    return db.Categories;
        //}

        // GET: api/Categories
        public IQueryable<CategorieDTO> GetCategories()
        {
            var categories = from b in db.Categories
                             select new CategorieDTO()
                             {
                                 Id = b.Cat_id,
                                 Categorie = b.Categorie1
                             };

            return categories;
        }



        //// GET: api/Categories/5
        //[ResponseType(typeof(Categorie))]
        //public async Task<IHttpActionResult> GetCategorie(short id)
        //{
        //    Categorie categorie = await db.Categories.FindAsync(id);
        //    if (categorie == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(categorie);
        //}

        // GET: api/Categories/5
        [ResponseType(typeof(CategorieDTO))]
        public async Task<IHttpActionResult> GetCategorie(short id)
        {
            var productenlijst = await
                                    (
                                        from b in db.Artikels.Include(b => b.Categorie) //eager loading
                                        where (b.Cat_id == id)
                                        select new ArtikelDTO()
                                        {
                                            Cat_id = b.Cat_id,
                                            Artikel_id = b.Artikel_id,
                                            Artikel1 = b.Artikel1,
                                            Omschrijving =b.Omschrijving,
                                            Verkoopprijs = b.Verkoopprijs.Value,
                                            Instock = b.Instock.Value,
                                            Categorie = b.Categorie.Categorie1
                                        }
                                    ).ToListAsync();

            if (productenlijst == null)
            {
                return NotFound();
            }

            return Ok(productenlijst);
        }



        // PUT: api/Categories/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCategorie(short id, Categorie categorie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categorie.Cat_id)
            {
                return BadRequest();
            }

            db.Entry(categorie).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategorieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Categories
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> PostCategorie(Categorie categorie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Categories.Add(categorie);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = categorie.Cat_id }, categorie);
        }

        // DELETE: api/Categories/5
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> DeleteCategorie(short id)
        {
            Categorie categorie = await db.Categories.FindAsync(id);
            if (categorie == null)
            {
                return NotFound();
            }

            db.Categories.Remove(categorie);
            await db.SaveChangesAsync();

            return Ok(categorie);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategorieExists(short id)
        {
            return db.Categories.Count(e => e.Cat_id == id) > 0;
        }
    }
}