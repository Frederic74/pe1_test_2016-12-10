﻿using Newtonsoft.Json;          //Toegevoegd
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;          //Toegevoegd
using System.Threading.Tasks;   //Toegevoegd
using System.Web;
using System.Web.Mvc;
using test2WebApi.Models;       //Toegevoegd

namespace test2WebApi.Controllers
{
    public class HomeController : Controller
    {
        //public ActionResult Index()
        //{
        //    ViewBag.Title = "Home Page";

        //    return View();
        //}

        // GET: Home
        //      De method Index behandelt de standaard URL voor de HomeController klasse “/”
        //          get .../
        //          get .../Home
        public ActionResult Index()
        {
            string uri = "http://" + Request.Url.Host + ':' + Request.Url.Port + "/api/categories";
            using (HttpClient httpClient = new HttpClient())
            {
                Task<String> response = httpClient.GetStringAsync(uri);
                return View(Task.Factory.StartNew(() => JsonConvert.DeserializeObject<List<CategorieDTO>>(response.Result)).Result);
            }
        }



    }
}
