﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiLib;
using test2WebApi.Models;   //Toegevoegd om DTO klassen te kunnen aanspreken

namespace test2WebApi.Controllers
{
    public class ArtikelsController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/Artikels
        public IQueryable<Artikel> GetArtikels()
        {
            return db.Artikels;
        }



        //// GET: api/Artikels/5
        //[ResponseType(typeof(Artikel))]
        //public async Task<IHttpActionResult> GetArtikel(short id)
        //{
        //    Artikel artikel = await db.Artikels.FindAsync(id);
        //    if (artikel == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(artikel);
        //}

        // GET: api/Artikels/5
        [ResponseType(typeof(ArtikelDTO))]
        public async Task<IHttpActionResult> GetArtikels(short id)
        {
            var product = await
                                (
                                    from b in db.Artikels.Include(b => b.Categorie) //eager loading
                                    where (b.Artikel_id == id)
                                    select new ArtikelDTO()
                                    {
                                        Cat_id = b.Cat_id,
                                        Artikel_id = b.Artikel_id,
                                        Artikel1 = b.Artikel1,
                                        Omschrijving = b.Omschrijving,
                                        Verkoopprijs = b.Verkoopprijs.Value,
                                        Instock = b.Instock.Value,
                                        Categorie = b.Categorie.Categorie1
                                    }
                                ).FirstOrDefaultAsync();

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }











        // PUT: api/Artikels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutArtikel(short id, Artikel artikel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != artikel.Artikel_id)
            {
                return BadRequest();
            }

            db.Entry(artikel).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArtikelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Artikels
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> PostArtikel(Artikel artikel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Artikels.Add(artikel);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = artikel.Artikel_id }, artikel);
        }

        // DELETE: api/Artikels/5
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> DeleteArtikel(short id)
        {
            Artikel artikel = await db.Artikels.FindAsync(id);
            if (artikel == null)
            {
                return NotFound();
            }

            db.Artikels.Remove(artikel);
            await db.SaveChangesAsync();

            return Ok(artikel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArtikelExists(short id)
        {
            return db.Artikels.Count(e => e.Artikel_id == id) > 0;
        }
    }
}