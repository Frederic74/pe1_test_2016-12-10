﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace test2WebApi.Models
{

    public class CategorieDTO
    {
        public int Id { get; set; }
        public string Categorie { get; set; }
     }

    public class ArtikelDTO
    {
        public short Artikel_id { get; set; }
        public short Cat_id { get; set; }
        public string Artikel1 { get; set; }
        public string Omschrijving { get; set; }
        public decimal Verkoopprijs { get; set; }
        public short Instock { get; set; }

        public string Categorie { get; set; }
    }

}



